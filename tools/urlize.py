#!/usr/bin/env python
import os
import sys
from slugify import slugify

def urlize_file(file):
	name, ext = os.path.splitext(file)
	new_name = slugify(name)

	if new_name != name:
		return new_name, ext
	else:
		return None, None

def rename_file(directory, file):
	new_name, file_ext = urlize_file(file)

	if new_name is not None:
		new_file = new_name + file_ext
		old_path = os.path.join(directory, file)
		new_path = os.path.join(directory, new_file)

		os.rename(old_path, new_path)
		print(f"Renamed {file} to {new_file}")

def rename_files_in_dir(directory):
	for file in os.listdir(directory):
		# ignore subdirs
		if os.path.isdir(os.path.join(directory, file)):
			continue
		rename_file(directory, file)

def main():
	if len(sys.argv) != 2:
		print("Usage: ./urlize.py <directoryPath>")
		sys.exit(1)

	directory = sys.argv[1]
	if not os.path.isdir(directory):
		print(f"The specified path is not a directory: {directory}")
		sys.exit(1)

	rename_files_in_dir(directory)

if	__name__ == "__main__":
	main()
