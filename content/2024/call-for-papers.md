---
title: "Wiki Workshop 2024: Research Track Papers"
description: "We invite contributions to the Research Track of the 11th edition of Wiki Workshop, which will take place virtually on June 20, 2024 as a standalone event."
---

{{< section title="Call for contributions" id="call">}}
We invite contributions to the Research Track of the 11th edition
of Wiki Workshop, which will take place virtually on June 20, 2024
(tentatively 12:00-19:00 UTC) as a standalone event.

The Wiki Workshop is the largest Wikimedia research event of the year, aimed at
bringing together researchers who study all aspects of Wikimedia projects
(including, but not limited to, Wikipedia, Wikidata, Wikimedia Commons,
Wikisource, and Wiktionary) as well as Wikimedia developers, affiliate
organizations, and volunteer editors. Co-organized by the Wikimedia Foundation’s
Research team and members of the Wikimedia research community, the workshop
provides a direct pathway for exchanging ideas between the organizations that
serve Wikimedia projects and the researchers actively studying them.

Building on the successful experiences of organizing Wiki Workshop in [2015](/2015), [2016](/2016), [2017](/2017),
[2018](/2018), [2019](/2019), [2020](/2020), [2021](/2021), [2022](/2022), [2023](/2023), and based on feedback from authors
and participants over the years, this year’s Research Track is organized as follows:

- Submissions are non-archival, meaning we welcome ongoing, completed, and
  already published work.
- We accept submissions in the form of 2-page extended abstracts.
- Authors of accepted abstracts will be invited to present their research in a
  pre-recorded oral presentation with dedicated time for live Q&A on June
  20, 2024.
- Accepted abstracts will be shared on the website prior to the event.

**Topics** include, but are not limited to:

- new technologies and initiatives to grow content, quality, equity, diversity,
  and participation across Wikimedia projects;
- use of bots, algorithms, and crowdsourcing strategies to curate, source, or
  verify content and structured data;
- bias in content and gaps of knowledge on Wikimedia projects;
- relation between Wikimedia projects and the broader (open) knowledge
  ecosystem;
- exploration of what constitutes a source and how/if the incorporation of other
  kinds of sources are possible (e.g., oral histories, video);
- detection of low-quality, promotional, or fake content (misinformation or
  disinformation), as well as fake accounts (e.g., sock puppets);
- questions related to community health (e.g., sentiment analysis, harassment
  detection, tools that could increase harmony);
- motivations, engagement models, incentives, and needs ofeditors, readers,
  and/or developers of Wikimedia projects;
- innovative uses of Wikipedia and other Wikimedia projects for AI and NLP
  applications and vice versa;
- consensus-finding and conflict resolution on editorial issues;
- dynamics of content reuse across projects and the impact of policies and
  community norms on reuse;
- privacy, security, and trust;
- collaborative content creation;
- innovative uses of Wikimedia projects' content and consumption patterns as
  sensors for real-world events, culture, etc.;
- open-source research code, datasets, and tools to support research on
  Wikimedia contents and communities;
- connections between Wikimedia projects and the Semantic Web;
- strategies for how to incorporate Wikimedia projects into media literacy
  interventions.
{{< /section >}}

{{< section title="Submission instructions" id="submission">}}
Following the success of last year’s updated submission format,
this year’s Wiki Workshop solicits extended abstracts (PDF format,
maximum 2 pages, including references). Submissions that exceed
the 2-page limit will be automatically rejected. Authors may
include 1 additional page with figures and/or tables (including
captions) only. Initial submissions require names and affiliations
of authors, 5 keywords, a title, an abstract, and a main text
outlining the contribution, methods, findings, and impact of the
work, whichever is relevant. Submissions will be non-archival and,
as a result, may have already been published, under review, or
ongoing research. All submissions will be reviewed by multiple
members of the Wiki Workshop Program Committee. The names of the
authors will be revealed to the reviewers, whereas reviewers will
remain anonymous to the authors.

Please review our [Privacy
Statement](https://foundation.wikimedia.org/wiki/Legal:Wiki_Workshop_Privacy_Statement)
before submitting your abstract to OpenReview.

- [Template for submissions](https://gitlab.wikimedia.org/repos/research/wikiworkshop-templates)
- [Submission site](https://openreview.net/group?id=wikimedia.org/Wiki_Workshop/2024/Research_Track)
{{< /section >}}

{{< section title="Key dates" id="dates">}}
- Submission deadline: <time datetime="2024-04-22">April 22, 2024 (23:59 [AoE](https://en.wikipedia.org/wiki/Anywhere_on_Earth))</time>
- Author notification: <time datetime="2024-05-27">May 27, 2024</time>
- Final version due: <time datetime="2024-06-10">June 10, 2024 (23:59 AoE)</time>
- Workshop date: <time datetime="2024-06-20">June 20, 2024 (tentatively scheduled from 12:00 to 19:00 UTC)</time>
{{< /section >}}

{{< section title="Contact">}}
For questions, send an email to wikiworkshop@googlegroups.com with a `[Research Track]` tag in the subject line.

**_PC-chairs,<br>
Pablo Beytía and Martin Gerlach_**
{{< /section >}}
