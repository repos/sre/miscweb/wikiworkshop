---
title: "Wiki Workshop 2024: Hall"
description: "We invite your contributions to the Wiki Workshop Hall, a new track as part of Wiki Workshop 2024 which will take place virtually on June 20, 2024. The Hall is a novel space for researchers to connect with the Wikimedia Movement members and for the Wikimedia Movement to connect with Wikimedia researchers."
---

{{< section title="Call for proposals" id="call">}}
We invite your contributions to the Wiki Workshop Hall, a new track as part of
Wiki Workshop 2024 which will take place virtually on June 20, 2024 (tentatively
12:00-19:00 UTC). The Hall is a novel space for researchers to connect with the
Wikimedia Movement members and for the Wikimedia Movement to connect with
Wikimedia researchers. Through this new track, we aim to provide a dedicated
space for learning, exchange of ideas, the spark of curiosity, and community
building.

Wiki Workshop is the largest Wikimedia Research event of the year, aimed at
bringing together researchers who study all aspects of Wikimedia projects
(including, but not limited to, Wikipedia, Wikidata, Wikimedia Commons,
Wikisource, and Wiktionary) as well as Wikimedia developers, affiliate
organizations, and volunteer editors. Co-organized by the Wikimedia Foundation’s
Research team and members of the Wikimedia Research community, the workshop
provides a direct pathway for exchanging ideas between the organizations that
serve Wikimedia projects and the researchers actively studying them.

We welcome proposals from both researchers and non-researchers that align the
interactive and collaborative spirit of the Wiki Workshop Hall and look forward
to a wide variety of content: experiences and learnings, knowledge pieces,
how-tos, open questions, pain points, etc. During the Hall, a breakout room will
be set up for each accepted proposal, so that Wiki Workshop attendees can move
between rooms to interact with their hosts.

On the bottom of this page, we provide a section with example summary proposals
for inspiration.
{{< /section >}}

{{< section title="Submission instructions" id="submission">}}
Proposals must be submitted using the online form on [OpenReview](https://openreview.net/group?id=wikimedia.org/Wiki_Workshop/2024/Hall#tab-recent-activity). You can view
the submission form in [advance](https://docs.google.com/document/d/1dKyd8CxM7sJvmVU-Vaj4iusnxR_x0KFDCo81VnGgMOE/).

Wiki Workshop Hall chairs will select proposals based on their potential to
contribute to community building, knowledge sharing, sparking curiosity and
engaging with Wikimedia researchers. Priority will be given to proposals that
are assessed to contribute to a greater diversity of topics, presenters, and
functional roles. By default, Wiki Workshop Hall sessions are allotted 30
minutes each; however, we are open to submissions of different durations.

Submissions will not be publicly available on OpenReview. Please review our
[Privacy Statement](https://foundation.wikimedia.org/wiki/Legal:Wiki_Workshop_Privacy_Statement)
before submitting your proposal.
{{< /section >}}

{{< section title="Important dates and timeline" id="dates">}}
- Submission deadline: <time datetime="2024-04-29">[April 29, 2024 (23:59)](https://www.timeanddate.com/worldclock/converter.html?iso=20240430T115900&p1=tz_aoe)</time>
- Author notification: <time datetime="2024-05-13">May 13, 2024</time>
- Final material (slides, pre-recorded videos, etc.) submission deadline:: <time datetime="2024-06-06">June 6, 2024 (23:59 AoE)</time>
- Workshop date: <time datetime="2024-06-20">June 20, 2024 (tentatively scheduled from 12:00 to 19:00 UTC)</time>
{{< /section >}}

{{< section title="Wiki Workshop Hall chairs" id="chairs">}}
- Kinneret Gordon _(Wikimedia Foundation)_
- Pablo Aragón _(Wikimedia Foundation)_
{{< /section >}}

{{< section title="Contact">}}
If you have questions or need guidance in preparing your proposal, send an email
to wikiworkshop@googlegroups.com with a `[WikiWorkshop Hall]` tag in the subject
line.
{{< /section >}}

{{< section title="FAQs">}}
#### How is the Wiki Workshop Hall different from the Research track?

The Research track is the original track of Wiki Workshop in which researchers
present and discuss ongoing and published work on Wikimedia projects. The Wiki
Workshop Hall is a new track for any member of the Wikimedia Movement to engage
with Wiki Workshop attendees by sharing a challenge, question, experience, idea,
or insight of interest to the Wikimedia Research community.

While the Research track sessions will consist of a series of presentations and
Q&A, the Wiki Workshop Hall session is intended to be more dynamic with
interactive formats suggested by the proposals’ authors.

#### Can I submit proposals to the Hall and the Research track?

Yes, if you need guidance in selecting where to submit your proposal, send an
email to wikiworkshop@googlegroups.com with a `[WikiWorkshop Hall]` tag in the
subject line.

#### Can I submit multiple sessions to the Hall?

Yes, but keep in mind that sessions happen simultaneously, so we may be unable
to accept more than one session.

#### What should I prepare for my session if my proposal gets accepted?

We will encourage authors of accepted proposals to prepare content for the Hall
(e.g., slides, pre-recorded videos, etc.). that will be submitted by June 6,
2024. During the Hall, each author of an accepted proposal will have a breakout
room to host their session and, if needed, share the screen to display content.

#### What technology platform will be used for the Hall?

Like the rest of Wiki Workshop sessions, the Hall will be virtually on Zoom.

#### What is the length of each Hall session?

Each session will be allotted 30 minutes by default. However, we are embracing
this track's dynamic nature and  will consider proposals for sessions of varying
lengths. Please explain how you plan to use your time in the submission form on
OpenReview.

#### Who will my audience be?

Wiki Workshop is open to anyone, but most attendees are researchers studying
Wikimedia projects (e.g., Wikipedia, Wikidata, Commons, etc.). Other attendees
include Wikimedia developers, affiliate organizations, and volunteer editors.
{{< /section >}}

{{< section title="Examples for inspiration" id="inspiration">}}
##### Tools to maintain/improve Wikimedia projects

> “In this tutorial I will unravel how I created an anti-vandalism bot for Spanish
> Wikipedia. Discover the coding techniques, algorithmic strategies, and
> real-world deployment that make this bot effective. Join me for an insightful
> session that bridges theory and practice, providing you with lessons to combat
> online vandalism in Wikipedia with confidence.”

##### Open datasets that could enrich Wikimedia projects

> “Uncover the richness of indigenous oral traditions in our session, where I
> share insights into creating a unique media dataset. Explore the diverse
> cultural tapestry embedded in these files, offering a valuable resource to
> enhance Wikipedia content. Join us to learn how this dataset can contribute to a
> more inclusive representation of indigenous heritage.”

##### Programs of Wikimedia chapters, thematic organizations, or user groups

> “Discover the impactful initiatives driving change in the Wikimedia Movement in
> our panel discussion featuring leaders from various chapters, thematic
> organizations, and user groups. Gain insights into innovative projects,
> community-building efforts, and strategies for fostering collaboration across
> diverse regions and communities.”

##### Stories about events to coordinate knowledge creation on Wikimedia projects

> “Join me for an exclusive look behind the scenes of organizing Wikimedia events.
> As an event organizer, I'll share insights into coordinating logistics, engaging
> diverse participants, and overcoming challenges. Learn how these efforts
> contribute to building a stronger Wikimedia community.”

##### Case studies from GLAM Wiki communities

> “Engage in a session where I, Digital Librarian at the National Library of
> Freedonia, will share a series of case studies on how Galleries, Libraries,
> Archives, Museums (GLAM institutions) can contribute to and benefit from
> Wikimedia projects. Learn about best practices of sharing cultural treasures
> online and partnering with Wikimedia communities. Let's amplify the reach of our
> collective heritage!”

##### Wikipedia policy drafts to address new challenges.

> “Join us, a group of experienced Wikipedians, in exploring a policy draft just
> proposed to address the use of large language models on Italian Wikipedia. We’ll
> hold a round-table discussion and delve into the nuances of ethical
> implementation, community impact, and content quality. Your insights, as a
> researcher, are crucial in this collaborative effort.”

##### Case studies from GLAM Wiki communities

> “Engage in a session where I, Digital Librarian at the National Library of
> Freedonia, will share a series of case studies on how Galleries, Libraries,
> Archives, Museums (GLAM institutions) can contribute to and benefit from
> Wikimedia projects. Learn about best practices of sharing cultural treasures
> online and partnering with Wikimedia communities. Let's amplify the reach of our
> collective heritage!”

##### Wikipedia policy drafts to address new challenges.

> “Join us, a group of experienced Wikipedians, in exploring a policy draft just
> proposed to address the use of large language models on Italian Wikipedia. We’ll
> hold a round-table discussion and delve into the nuances of ethical
> implementation, community impact, and content quality. Your insights, as a
> researcher, are crucial in this collaborative effort.”

##### Public policies affecting the Wikimedia ecosystem.

> “As a digital rights activist, I'll dissect the implications of the European
> Digital Services Act. Explore in this session how this legislation could shape
> content moderation, user privacy, and online freedoms in Wikimedia projects.
> Join us for a crucial discussion on safeguarding the principles integral to the
> Wikimedia community in the evolving landscape of digital governance.”

##### Insightful experiences from Wikimedians (admins, patrollers, media uploaders, etc.).

> “As a Wikimedia editor with a deep passion for photography, I've traveled
> extensively, capturing stunning images for Wikimedia Commons. In my booth, I'll
> share the stories behind some of my favorite photographs, discuss the challenges
> of documenting diverse cultures, and explore how visual storytelling enriches
> Wikimedia projects.”
{{< /section >}}
