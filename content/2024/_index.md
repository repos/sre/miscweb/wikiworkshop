---
title: "Wiki Workshop 2024"
description: "A forum bringing together researchers exploring all aspects of Wikimedia projects. Held virtually as a standalone event, June 20, 2024"
---

{{< section title="News">}}
- <time datetime="2024-07-08">July 8, 2024</time>: Extended abstract PDFs, keynote slides PDF, and video recordings are now available.
- <time datetime="2024-05-27">May 27, 2024</time>: The workshop schedule is published.
- <time datetime="2024-04-30">April 30, 2024</time>: Registration for Wiki Workshop 2024 is now open. [Register on Pretix!](https://pretix.eu/wikimedia/wikiworkshop2024/)
- <time datetime="2024-03-06">March 6, 2024</time>: Call for proposals for Wiki Workshop Hall published.
- <time datetime="2024-02-15">February 15, 2024</time>: Keynote speaker is announced!
- <time datetime="2024-01-30">January 30, 2024</time>: OpenReview link and submission template are published.
- <time datetime="2024-01-29">January 29, 2024</time>: Key dates, call for contributions and submission instructions are published.
- <time datetime="2024-01-15">January 15, 2024</time>: Wiki Workshop 2024 webpage online.
- <time datetime="2024-01-15">January 15, 2024</time>: Wiki Workshop 2024 will be fully remote.
{{< /section >}}

{{< section title="Schedule">}}

The times in the table below are in UTC. 12:00 UTC is 5:00 in San Francisco, 8:00 in New York City, 15:00 in Nairobi, and 20:00 in Beijing.

| | |
| --- | --- |
| <time datetime="12:00">12:00</time> - <time datetime="12:15">12:15</time> | Welcome and Orientation |
| <time datetime="12:15">12:15</time> - <time datetime="12:25">12:25</time> | Getting to Know Each Other |
| <time datetime="12:25">12:25</time> - <time datetime="13:45">13:45</time> | Research track *(parallel sessions)* |
| <time datetime="13:45">13:45</time> - <time datetime="13:50">13:50</time> | Break |
| <time datetime="13:50">13:50</time> - <time datetime="14:00">14:00</time> | Live Music |
| <time datetime="14:00">14:00</time> - <time datetime="14:15">14:15</time> | Wikimedia Foundation Research Award of the Year ceremony |
| <time datetime="14:15">14:15</time> - <time datetime="15:00">15:00</time> | Wiki Workshop Hall *(parallel sessions)* |
| <time datetime="15:00">15:00</time> - <time datetime="15:10">15:10</time> | Break |
| <time datetime="15:10">15:10</time> - <time datetime="16:25">16:25</time> | Research track *(parallel sessions)* |
| <time datetime="16:25">16:25</time> - <time datetime="16:30">16:30</time> | Break |
| <time datetime="16:30">16:30</time> - <time datetime="17:30">17:30</time> | Keynote Presentation |
| <time datetime="17:30">17:30</time> - <time datetime="17:35">17:35</time> | Break |
| <time datetime="15:35">17:35</time> - <time datetime="17:45">17:45</time> | Live Music |
| <time datetime="15:45">17:45</time> - <time datetime="18:20">18:20</time> | Wiki Workshop Hall *(parallel sessions)* |
| <time datetime="18:20">18:20</time> - <time datetime="18:55">18:55</time> | Town hall and open conversation |
| <time datetime="18:55">18:55</time> - <time datetime="19:00">19:00</time> | Closing |
{{< /section >}}

{{< keynote title="Keynote" id="keynote" >}}

{{< papers title="Accepted extended abstracts" id="papers" >}}

{{< papers title="Accepted Hall sessions" id="hall" >}}

{{< persons title="Program committee" id="program-committee" sort="lastName" >}}

{{< section title="Key dates" id="dates">}}
- Submission deadline: <time datetime="2024-04-22">April 22, 2024 (23:59 [AoE](https://en.wikipedia.org/wiki/Anywhere_on_Earth))</time>
- Author notification: <time datetime="2024-05-27">May 27, 2024</time>
- Final version due: <time datetime="2024-06-10">June 10, 2024 (23:59 AoE)</time>
- Workshop date: <time datetime="2024-06-20">June 20, 2024 (tentatively scheduled from 12:00 to 19:00 UTC)</time>
{{< /section >}}

{{< section title="Call for contributions" id="call">}}
- [Call for Research track papers](call-for-papers)
- [Call for Hall proposals](call-for-hall)
{{< /section >}}

{{< section title="Previous editions" id="previous">}}
- [@ Standalone Event 2023 (virtual)](/2023/)
- [@ WWW 2022 (virtual)](/2022/)
- [@ WWW 2021 (virtual)](/2021/)
- [@ WWW 2020 (Taipei, Taiwan)](/2020/)
- [@ WWW 2019 (San Francisco, USA)](/2019/)
- [@ WWW 2018 (Lyon, France)](/2018/)
- [@ WWW 2017 (Perth, Australia)](/2017/)
- [@ WWW 2016 (Montreal, Canada) and ICWSM 2016 (Cologne, Germany)](/2016/)
- [@ ICWSM 2015 (Oxford, UK)](/2015/)
{{< /section >}}

{{< persons title="Organization" id="organization" >}}

{{< section title="Contact" >}}
Please direct your questions to wikiworkshop@googlegroups.com.
{{< /section >}}
