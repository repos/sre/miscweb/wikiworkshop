# Wikimedia Research Landing Page

View at https://wikiworkshop.org

## Content management

### Papers

Metadata is stored at `data/papers.yaml` and PDFs and videos at `assets/papers/`. Use the paper title as the name for those files and run **urlize** Python script to rename them to a URL slug format.

## Development

This project uses [Hugo](https://gohugo.io/) static site generator as framework and [Blubber](https://wikitech.wikimedia.org/wiki/Blubber) for container configuration.

### Local development

You should have NPM and [Hugo](https://gohugo.io/installation/) installed.

Install Node dependencies:

```sh
npm install
```

Serve site:

```sh
hugo serve
```

Visit `http://localhost:1313/`

### Local development (with docker)

Build image locally:

```sh
DOCKER_BUILDKIT=1 docker build --tag wmf/wikiworkshop --target production -f .pipeline/blubber.yaml .
```

Run image:

```sh
docker run -ti -p 8080:8080 wmf/wikiworkshop
```

Visit `http://localhost:8080/`

### Publish new image version

To create a new image version merge your change into the master branch.

This triggers the publish-image pipeline. Image is available at `docker-registry.wikimedia.org/repos/sre/miscweb/wikiworkshop:<timestamp>`

### Deploy changes

- Merge changes to master branch.

- Wait for CI pipeline to finish and check latest tag on the [publish job log](https://gitlab.wikimedia.org/repos/sre/miscweb/research-landing-page/-/jobs/) _(check the `#13` step)_. Alternatively the [docker registry](https://docker-registry.wikimedia.org/repos/sre/miscweb/wikiworkshop/tags/) can be checked but it takes a long time to update.

- Update image version on [deployment-charts](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/deployment-charts/%2B/refs/heads/master/helmfile.d/services/miscweb/values-wikiworkshop.yaml#3) with the latest tag.

- Add **wmf-sre-collab** group as reviewer and wait until the change is merged.

- Deploy to production from the deployment server.

See [WikiTech: Miscweb](https://wikitech.wikimedia.org/wiki/Miscweb#Deploy_to_Kubernets).
