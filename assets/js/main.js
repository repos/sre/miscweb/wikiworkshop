import { ScrollSpy } from "bootstrap";

if (document.readyState === "loading")
	document.addEventListener("DOMContentLoaded", load, { passive: true });
else load();

function load() {
	loadBootstrap();
}

function loadBootstrap() {
	const main = document.querySelector("main");

	const scrollSpy = new ScrollSpy(main, {
		target: "#sidebar",
	});
}
